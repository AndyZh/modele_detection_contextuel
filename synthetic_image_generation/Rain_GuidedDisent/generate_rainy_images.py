#!/usr/bin/env python3

import os
import torch
from MUNIT.model_infer import MUNIT_infer
import yaml
from droprenderer import DropModel
from torchvision.transforms import ToTensor, ToPILImage
from PIL import Image
import imagesize
import glob
import warnings

warnings.filterwarnings("ignore", category=UserWarning, message=".*align_corners.*")

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def get_config(config):
    with open(config, 'r') as stream:
        return yaml.load(stream, Loader=yaml.FullLoader)


def init_model():
    hyperparameters = get_config('configs/params_net.yaml')
    model = MUNIT_infer(hyperparameters)
    weights = torch.load('weights/pretrained.pth')
    model.gen_a.load_state_dict(weights['a'])
    model.gen_b.load_state_dict(weights['b'])
    model = model.to(device)
    return model


def apply_fn(*inputs):
    input_image = inputs[0]

    img_resize_width = inputs[1]
    img_resize_height = inputs[2]

    input_image = input_image.resize((img_resize_width, img_resize_height), Image.BILINEAR)

    drops_size = inputs[3]
    drops_frequency = inputs[4]

    drops_shape = inputs[5]
    drops_sigma = torch.zeros(1).fill_(inputs[6]).to(device)

    dm = DropModel(imsize=(img_resize_height, img_resize_width),
                   size_threshold=1 / float(drops_size),
                   frequency_threshold=drops_frequency,
                   shape_threshold=drops_shape)
    model = init_model()

    im = ToTensor()(input_image) * 2 - 1
    im = im.unsqueeze(0).cuda()
    im_res = model.forward(im)
    im_drops = dm.add_drops(im_res, sigma=drops_sigma)

    im_res = ToPILImage()((im_res[0].cpu() + 1) / 2)
    im_drops = ToPILImage()((im_drops[0].cpu() + 1) / 2)

    return im_res, im_drops


def generate_rainy_images():
    # Reset the result folder
    result_folder = "results"
    os.system(f"rm -r {result_folder}")
    os.mkdir(result_folder)
    os.mkdir(f"{result_folder}/generated_rain_wet/")
    os.mkdir(f"{result_folder}/generated_rain_drop/")
    
    images_to_pre_proces_dir = "images_to_pre_process"
    nb_images = len(glob.glob(f"{images_to_pre_proces_dir}/*"))
    for i, image_path in enumerate(glob.glob(f"{images_to_pre_proces_dir}/*")):
        resized = False
        old_width, old_height = imagesize.get(image_path)
        print(f"Processing {i+1}/{nb_images}: {image_path}, Image size : {old_width}x{old_height}")
        
        # Resize of the images
        if old_height < 400:  # Weather kitti images
            resize_image_command = f"mogrify -resize 1240x360! {image_path}"
            os.system(resize_image_command)
            resized = True
        elif old_width*old_height == 2048*1024 or old_width*old_height == 1920*1080:
            resize_image_command = f"mogrify -resize 50% {image_path}"
            os.system(resize_image_command)
            resized = True
        elif old_width*old_height == 1280*720:
            resize_image_command = f"mogrify -resize 75% {image_path}"
            os.system(resize_image_command)
            resized = True

        image = Image.open(image_path)
        width, height = image.size

        image_input = (image, width, height, 50, 8, 0.6, 4)

        rainy_image, raindrop_image = apply_fn(*image_input)
        rainy_image = rainy_image.resize((old_width, old_height))
        raindrop_image = raindrop_image.resize((old_width, old_height))

        wet_image_name = image_path.split("/")[-1].replace(".jpg", "_rain_wet.jpg")
        rainy_image.save(f"{result_folder}/generated_rain_wet/{wet_image_name}")

        drop_image_name = image_path.split("/")[-1].replace(".jpg", "_rain_drop.jpg")
        raindrop_image.save(f"{result_folder}/generated_rain_drop/{drop_image_name}")

        if resized:
            resize_image_command = f"mogrify -resize {old_width}x{old_height}! {image_path}"
            os.system(resize_image_command)
            

if __name__ == '__main__':
    generate_rainy_images()
