#!/usr/bin/env bash

sudo docker run --gpus='"device=0"' -v $(pwd):/synthetic_rain_generation --shm-size=12G synthetic_rain_generation
