# Stage - Modèle de détection contextuel

## Résumé

Ce dépôt GitHub a été utilisé lors du stage pour générer des images synthétiques sous le brouillard.

Le lien vers les Git originaux sont disponibles ici :

- [Monodepth2](https://github.com/nianticlabs/monodepth2) (Depth estimation)
- [FoHIS](https://github.com/noahzn/FoHIS) (Génération de brouillard synthétique utilisant les depth maps des images)

## Guide d'utilisation

Les images originales (de beaux temps) doivent être placées dans le dossier images_to_pre_process/.
Les images de brouillard synthétiques seront dans le dossier results/.

Afin de réaliser la génération des synthétiques à partir d'images de jour et de beau temps, il suffit de construire le docker et de l'exécuter :

```
./build_docker.sh
./run_docker.sh
```

L'exécution du docker lancera directement le code de génération des images synthétiques.

Note : Il est possible de choisir les intensités de brouillard que nous souhaitons générer. Il suffit de modifier la variable globale `FOG_DISTANCE_VIEWS` dans le script `fog.py` à l'emplacement suivant : `FoHIS/FoHIS/fog.py`

Cette variable correspond à une liste de distance de vue, chaque distance générera un set d'intensité d'images. 

Plus la distance de vue est petite, plus le brouillard généré est épais.

## Exemple

<table>
  <tr>
    <td colspan="3" align="center"><p><b>Synthetic Fog Image Samples</b></p></td>
  </tr>
  <tr>
    <td align="center"><p><b>Original Image (Sunny Day)</b></p></td>
    <td align="center"><p><b>Synthetic Fog Image 5000 view distance</b></p></td>
    <td align="center"><p><b>Synthetic Fog Image 2000 view distance</b></p></td>
  </tr>
  <tr>
    <td align="center"> <img src = "synthetic_image_generation/Fog_Synthetic/images/sunny_1.jpeg" width="400"> </td>
    <td align="center"> <img src = "synthetic_image_generation/Fog_Synthetic/images/sunny_1_5000.jpeg" width="400"> </td>
    <td align="center"> <img src = "synthetic_image_generation/Fog_Synthetic/images/sunny_1_2000.jpeg" width="400"> </td>
  </tr>
  <tr>
    <td align="center"> <img src = "synthetic_image_generation/Fog_Synthetic/images/sunny_2.jpeg" width="400"> </td>
    <td align="center"> <img src = "synthetic_image_generation/Fog_Synthetic/images/sunny_2_5000.jpeg" width="400"> </td>
    <td align="center"> <img src = "synthetic_image_generation/Fog_Synthetic/images/sunny_2_2000.jpeg" width="400"> </td>
  </tr>
  </tr>
</table>