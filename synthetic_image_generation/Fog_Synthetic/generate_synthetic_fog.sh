#!/bin/sh

# Depth map generation
python monodepth2/test_simple.py --image_path images_to_pre_process/ --model_name mono_1024x320

# Fog generation using the depth map
python FoHIS/FoHIS/fog.py