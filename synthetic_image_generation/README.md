# Stage - Modèle de détection contextuel

Ce dossier contient l'ensemble des repository de génération d'images synthétiques (image-to-image translation)

Le guide d'utilisation de chaque repository est expliqué au début des README de chaque dossier.

L'ensemble des modèles ont été dockerisé et peuvent être exécutés simplement.