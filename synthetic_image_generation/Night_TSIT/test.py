import os
from collections import OrderedDict

import data
from options.test_options import TestOptions
from models.pix2pix_model import Pix2PixModel
from util.visualizer import Visualizer
from util import html
from tqdm import tqdm
import imagesize
import warnings

warnings.filterwarnings("ignore", category=UserWarning, message="nn.functional.tanh is deprecated. Use torch.tanh instead.")

# Empty result folder
output_folder = "results/"
os.system(f"rm -r {output_folder}")
os.mkdir(output_folder)

opt = TestOptions().parse()

opt.crop_size = 1280

dataloader = data.create_dataloader(opt)

model = Pix2PixModel(opt)
if opt.task != 'MMIS' and opt.dataset_mode != 'photo2art':
    model.eval()

visualizer = Visualizer(opt)

web_dir = os.path.join(opt.results_dir, opt.name,
                       '%s_%s' % (opt.phase, opt.which_epoch))
webpage = html.HTML(web_dir,
                    'Experiment = %s, Phase = %s, Epoch = %s' %
                    (opt.name, opt.phase, opt.which_epoch))

# test
print('Number of images: ', len(dataloader))
for i, data_i in enumerate(tqdm(dataloader)):
    if i * opt.batchSize >= opt.how_many:
        break

    generated = model(data_i, mode='inference')

    img_path = data_i['cpath']
    for b in range(generated.shape[0]):

        if opt.show_input:
            if opt.task == 'SIS':
                visuals = OrderedDict([('synthesized_image', generated[b])])
            else:
                visuals = OrderedDict([('synthesized_image', generated[b])])
        else:
            visuals = OrderedDict([('synthesized_image', generated[b])])

        visualizer.save_images(webpage, visuals, img_path[b:b + 1])

        # Resize the image to have the original size
        current_img_path = img_path[b:b + 1][0]
        width, height = imagesize.get(current_img_path)
        image_name = current_img_path.split("/")[-1].split(".")[0]

        result_img_path = f"./results/ast_day2night/test_latest/images/{image_name}.png"
        
        resize_command = f"mogrify {result_img_path} -resize {width}x{height}! {result_img_path}"
        os.system(resize_command)

# Reorganizing the result folder
move_images_command = "mv ./results/ast_day2night/test_latest/images/* ./results"
remove_ast_2day_night_folder = "rm -r ./results/ast_day2night"

os.system(move_images_command)
os.system(remove_ast_2day_night_folder)

#webpage.save()
