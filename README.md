# Modèle de Détection Contextuel

Ce repository contient l'ensemble des repository publics qui ont été utilisés lors du stage Modèle de détection contextuel pour effectuer du pré-processing d'images lors de l'application des différentes méthodes d'amélioration - [Page confluence](https://xxiigroup.atlassian.net/wiki/spaces/REP/pages/2405859359/5.+M+thodes+d+am+lioration) :

- [a. Pre-processing avant inférence](https://xxiigroup.atlassian.net/wiki/spaces/REP/pages/2405859383/a.+M+thodes+de+pr+-processing+des+donn+es+avant+d+tection) :
    - :night_with_stars: [Night Image Enhancement](pre_process_before_detection/Night_LLFlow)
    - :cloud_with_rain: [Rain Removal (Deraining)](pre_process_before_detection/Rain_MPRNet)
    - :cloud_with_snow: [Snow Removal (Desnowing)](pre_process_before_detection/Snow_Two-stage-Knowledge)
    - :fog: [Fog Removal (Defogging)](pre_process_before_detection/Fog_DehazeFormer)

- [b. Pre-processing avant entraînement (Génération d'images synthétiques)](https://xxiigroup.atlassian.net/wiki/spaces/REP/pages/2405761099/b.+M+thodes+de+pr+-processing+des+donn+es+d+entra+nement+l+aide+de+l+image-to-image+translation) :
    - :night_with_stars: [Synthetic Night Generation](synthetic_image_generation/Night_TSIT/)
    - :cloud_with_rain: [Synthetic Rain Generation](synthetic_image_generation/Rain_GuidedDisent/)
    - :fog: [Synthetic Fog Generation (with depthmap generation)](synthetic_image_generation/Fog_Synthetic)

Note : Les différents scripts (build et run des dockers) sont à exécuter sur GARR-E (ou Patrick).
