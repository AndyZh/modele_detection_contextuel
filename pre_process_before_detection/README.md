# Stage - Modèle de détection contextuel

Ce dossier contient l'ensemble des repository de pré-processing des images.

Le guide d'utilisation de chaque repository est expliqué au début des README de chaque dossier.

L'ensemble des modèles ont été dockerisé et peuvent être exécutés simplement.