#!/usr/bin/env python

import os


def preprocess_night():
    """
    Enhance all the night images in images_to_pre_process folder
    Args:
    Output:
        The preprocess images are stored in results/pre_process_night_images
    """
    # Refresh output folder (delete and recreate)
    output_folder = f"results/"
    os.system(f"rm -r {output_folder}")
    os.mkdir(output_folder)

    # Night enhancement of the images
    deraining_command = f"python code/test_unpaired.py " \
                        f"--opt code/confs/pre_process_night_images.yml " \
                        f"-n ."

    os.system(deraining_command)


if __name__ == "__main__":
    preprocess_night()
