# Stage - Modèle de détection contextuel

## Résumé

Ce dépôt GitHub a été utilisé lors du stage pour appliquer une méthode de Fog Removal (Defogging). 

[Il provient d'une démo Hugging Face, disponible ici.](https://huggingface.co/spaces/IDKiro/DehazeFormer_Demo)

[Le code de cette démo est disponible ici.](https://huggingface.co/spaces/IDKiro/DehazeFormer_Demo/tree/main)

[Le lien vers le Git original est disponible ici.](https://github.com/IDKiro/DehazeFormer).

## Guide d'utilisation

Les images qui doivent être pré-processées doivent être placées dans le dossier images_to_pre_process/.
Les images pré-processées seront dans le dossier results/pre_process_night_images.

Afin de réaliser le Fog Removal sur des images, il suffit de construire le docker et de l'exécuter :

```
./build_docker.sh
./run_docker.sh
```

L'exécution du docker lancera directement le code de pré-traitement.

## Exemple

<table>
  <tr>
    <td colspan="2" align="center"><p><b>Defogging Samples</b></p></td>
  </tr>
    <tr>
    <td align="center"><p><b>Before Fog removal</b></p></td>
    <td align="center"><p><b>After Fog removal</b></p></td>
  </tr>
  <tr>
    <td align="center"> <img src="pre_process_before_detection/Fog_DehazeFormer/images/fog_1.png" width="400"> </td>
    <td align="center"> <img src="pre_process_before_detection/Fog_DehazeFormer/images/fog_1_result.png" width="400"> </td>
  </tr>
  <tr>
    <td align="center"> <img src="pre_process_before_detection/Fog_DehazeFormer/images/fog_2.png" width="400"> </td>
    <td align="center"> <img src="pre_process_before_detection/Fog_DehazeFormer/images/fog_2_result.png" width="400"> </td>
  </tr>
</table>