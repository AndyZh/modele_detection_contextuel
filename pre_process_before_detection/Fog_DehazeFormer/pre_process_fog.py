import torch
import glob
import os
import time
import numpy as np

from PIL import Image
from models import dehazeformer

def get_images_list(dataset_path):
	"""
	This functions returns the list of images path of the dataset in dataset_path
	Args:
		dataset_path: the path of the dataset
	"""
	list_images = []
	for file in glob.glob(f"{dataset_path}/*"):
		list_images.append(file)
	return list_images

def prepare_result_folder():
	"""
	This function delete and recreates all the result folder
	"""
	os.system("rm -r results/")
	os.mkdir("results")


def infer(list_image_directory):
	"""
	This function loads the pre-trained model of fog removal and remove the fog of all the images in
	list_image_directory
	Args:
		list_image_directory: The list of all the image we want to remove fog
		dataset_name: the name of the dataset (corresponding to the result folder)
	"""
	device = torch.device("cuda")
	network = dehazeformer()
	network.load_state_dict(torch.load('./saved_models/dehazeformer.pth',
									   map_location=torch.device("cuda:0"))['state_dict'])
	network.to(device)
	network.eval()

	for image_directory in list_image_directory:
		print(f"Defogging : {image_directory}")

		raw_image = Image.open(image_directory)
		image = np.array(raw_image, np.float32) / 255. * 2 - 1
		image = torch.from_numpy(image)
		image = image.permute((2, 0, 1)).unsqueeze(0)
		image = image.to(device)

		with torch.no_grad():
			output = network(image).clamp_(-1, 1)[0] * 0.5 + 0.5
			output = output.permute((1, 2, 0))
			output = output.cpu()
			output = np.array(output, np.float32)
			output = np.round(output * 255.0)

		output = Image.fromarray(output.astype(np.uint8))

		image_name = image_directory.split("/")[-1]
		output_directory = f"results/{image_name}"
		output.save(output_directory)


if __name__ == "__main__":
	
	prepare_result_folder()

	dataset = "images_to_pre_process/"
	list_images = get_images_list(dataset)
	infer(list_images)
