#!/usr/bin/env python3

import os


def preprocess_rain():
    """
    Derain all the images using the MPRNet for all the images in the folder images_to_pre_process/
    Args:
    Output:
        The preprocess images are stored in results/
    """
    output_folder = f"results/"
    os.system(f"rm -r {output_folder}")
    os.mkdir(output_folder)

    input_folder = "images_to_pre_process/"

    deraining_command = f"python demo.py " \
                        f"--task Deraining " \
                        f"--input_dir {input_folder} " \
                        f"--result_dir {output_folder}"
    
    os.system(deraining_command)


if __name__ == "__main__":
    preprocess_rain()
