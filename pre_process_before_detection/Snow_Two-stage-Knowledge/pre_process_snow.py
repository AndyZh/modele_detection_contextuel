#!/usr/bin/env python

import os


def preprocess_snow():
    """
    Desnow all the images in images_to_pre_process folder
    Args:
    Output:
        The preprocess images are stored in results/
    """
    # Refresh output folder (delete and recreate)
    output_folder = f"results/"
    os.system(f"rm -r {output_folder}")
    os.mkdir(output_folder)
    
    # Desnowing the images
    desnowing_command = f"python inference.py " \
                        f"--dir_path images_to_pre_process " \
                        f"--checkpoint model_weights/Snow100k " \
                        f"--save_dir {output_folder}"

    os.system(desnowing_command)


if __name__ == "__main__":
    preprocess_snow()
